﻿using System.Diagnostics.CodeAnalysis;
using System.Text;
using Axle.Modularity;
using Maeson.Core.Tasks;

namespace Maeson.Console;

[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
[Module]
[ModuleCommandLineTrigger(ArgumentIndex = 0, ArgumentValue = "build")]
[RequiresMaesonTaskExecutor]
internal sealed class MaesonConsoleModule
{
    private readonly IMaesonTaskExecutor _maesonTaskExecutor;

    public MaesonConsoleModule(IMaesonTaskExecutor maesonTaskExecutor)
    {
        _maesonTaskExecutor = maesonTaskExecutor;
    }

    [ModuleInit]
    public void Init()
    {
        
    }

    [ModuleEntryPoint]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public void Run(string[] args)
    {
        var argString = args.Skip(1).Aggregate(new StringBuilder(args[0]), (sb, str) => sb.AppendFormat(", {0}", str));
        System.Console.WriteLine($"{nameof(MaesonConsoleModule)} invoked with args: {argString}");
        if (args.Length < 2)
        {
            System.Console.WriteLine("Insufficient command-line arguments");            
        }
        _maesonTaskExecutor.Execute(args[1]);
    }
}