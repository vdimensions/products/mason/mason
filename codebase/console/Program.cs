﻿using Axle.Application;
using Maeson.Console;

Application
    .Build()
    .ConfigureModules(m => m.Load<MaesonConsoleModule>())
    .Run(args);
