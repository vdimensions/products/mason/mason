using System.Runtime.Serialization;

namespace Maeson.Builders.VSCode;

[DataContract]
internal record TextMateTokenStyleSettings
{
    [field:DataMember(Name = "foreground", IsRequired = false, EmitDefaultValue = false)]
    public string? ForegroundColor { get; set; }
    
    [field:DataMember(Name = "fontStyle", IsRequired = false, EmitDefaultValue = false)]
    public string? FontStyle { get; set; }
}