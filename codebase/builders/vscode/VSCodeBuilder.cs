﻿using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using Axle.Collections.Generic.Extensions.KeyValuePair;
using Axle.Extensions.String;
using Axle.IO.Serialization;
using Axle.Modularity;
using Maeson.Core.Builders;

namespace Maeson.Builders.VSCode;

[Module]
[SuppressMessage("ReSharper", "UnusedType.Global")]
[SuppressMessage("ReSharper", "InconsistentNaming")]
internal sealed class VSCodeBuilder : IMaesonBuilder
{
    private const string PrefixTokenColors = "tokenColors.";
    private const string PrefixColors = "colors";
    private const string PrefixSemanticTokenColors = "semanticTokenColors.";

    private readonly DataContractJsonSerializer _themeSerializer;
    private readonly ISerializer _styleSerializer;
    
    public VSCodeBuilder()
    {
        var jsonSerializerSettings = new DataContractJsonSerializerSettings()
        {
            SerializeReadOnlyTypes = true,
            UseSimpleDictionaryFormat = true,
            EmitTypeInformation = EmitTypeInformation.Never,
        };
        _themeSerializer = new DataContractJsonSerializer(typeof(VSCodeTheme), jsonSerializerSettings);
        _styleSerializer = new JsonContractSerializer();
        Name = GetType().Name.TrimEnd("Builder", StringComparison.OrdinalIgnoreCase);
    }
    
    public void Build(IDictionary<string, string> document, Stream outputStream)
    {
        var result = new VSCodeTheme()
        {
            Colors = new Dictionary<string, string>(StringComparer.Ordinal),
            TokenColors = new List<TextMateTokenStyleRule>(),
            SemanticTokenColors = new Dictionary<string, TextMateTokenStyleSettings>(StringComparer.Ordinal),
        };
        ProcessColours(document, result.Colors);
        ProcessTokenColours(document, result.TokenColors);
        ProcessSemanticTokenColours(document, result.SemanticTokenColors);
        _themeSerializer.WriteObject(outputStream, result);
    }

    private IEnumerable<KeyValuePair<string, string>> FilterKeys(IEnumerable<KeyValuePair<string, string>> textDocument, Predicate<string> filter)
    {
        return textDocument.Where(pair => filter(pair.Key));
    }
    private IEnumerable<KeyValuePair<string, string>> ExtractSubKeys(IEnumerable<KeyValuePair<string, string>> textDocument, string prefix)
    {
        return textDocument
            .Where(pair => pair.Key.StartsWith(prefix, StringComparison.Ordinal))
            .Select(pair => pair.MapKey(key => key.TrimStart(prefix, StringComparison.Ordinal)));
    }

    private void ProcessColours(IEnumerable<KeyValuePair<string, string>> textDocument, Dictionary<string, string> result)
    {
        foreach (var (key, value) in FilterKeys(textDocument, k => k.StartsWith(PrefixColors, StringComparison.Ordinal)))
        {
            result.Add(key.TrimStart(PrefixColors).TrimStart('.'), value);
        }
    }

    private void ProcessTokenColours(IDictionary<string, string> textDocument, List<TextMateTokenStyleRule> result)
    {
        ExtractSubKeys(textDocument, PrefixTokenColors)
            .GroupBy(x => x.Value, x => (Scope: x.Key, Json: x.Value))
            .SelectMany(data => data)
            .AsParallel()
            .Select(
                tuple =>
                {
                    using var stream = new MemoryStream(Encoding.UTF8.GetBytes(tuple.Json));
                    var settings = (TextMateTokenStyleSettings) _styleSerializer.Deserialize(stream, typeof(TextMateTokenStyleSettings));
                    if (!CleanUp(settings))
                    {
                        return null;
                    }
                    return new TextMateTokenStyleRule
                    {
                        Scope = new List<string> { tuple.Scope },
                        Settings = settings
                    };
                })
            .Where(x => x != null)
            .GroupBy(x => x!.Settings, x => x.Scope[0])
            .Select(
                x => new TextMateTokenStyleRule
                {
                    Scope = x.ToList(),
                    Settings = x.Key
                })
            .ToList()
            .ForEach(result.Add);
    }

    private bool CleanUp(TextMateTokenStyleSettings settings)
    {
        if (string.IsNullOrEmpty(settings.FontStyle))
        {
            settings.FontStyle = null;
        }
        if (string.IsNullOrEmpty(settings.ForegroundColor))
        {
            settings.ForegroundColor = null;
        }
        return settings.ForegroundColor != null || settings.FontStyle != null;
    }

    private void ProcessSemanticTokenColours(IDictionary<string, string> textDocument, Dictionary<string, TextMateTokenStyleSettings> result)
    {
        ExtractSubKeys(textDocument, PrefixSemanticTokenColors)
            .AsParallel()
            .Select(
                pair =>
                {
                    return pair.MapValue(
                        json =>
                        {
                            using var stream = new MemoryStream(Encoding.UTF8.GetBytes(json));
                            var data = (TextMateTokenStyleSettings) _styleSerializer.Deserialize(stream, typeof(TextMateTokenStyleSettings));
                            if (CleanUp(data))
                            {
                                return data;
                            }
                            return null;
                        });
                })
            .Where(x => x.Value != null)
            .ToList()
            .ForEach(item => result.Add(item.Key, item.Value!));
    }

    public string Name { get; }
}
