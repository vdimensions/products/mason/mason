using System.Runtime.Serialization;

namespace Maeson.Builders.VSCode;

[DataContract]
internal record VSCodeTheme
{
    [field: DataMember(Name = "$schema")]
    public string Schema { get; init; } = "vscode://schemas/color-theme";
    
    [field:DataMember(Name = "colors")]
    public Dictionary<string, string> Colors { get; set; }
        
    [field:DataMember(Name = "tokenColors")]
    public List<TextMateTokenStyleRule> TokenColors { get; set; }
        
    [field:DataMember(Name = "semanticTokenColors")]
    public Dictionary<string, TextMateTokenStyleSettings> SemanticTokenColors { get; set; }
}