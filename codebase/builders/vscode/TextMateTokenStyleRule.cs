using System.Runtime.Serialization;

namespace Maeson.Builders.VSCode;

[DataContract]
internal record TextMateTokenStyleRule
{
    [field:DataMember(Name = "scope")]
    public List<string> Scope { get; set; }
        
    [field:DataMember(Name = "settings")]
    public TextMateTokenStyleSettings Settings { get; set; }
}