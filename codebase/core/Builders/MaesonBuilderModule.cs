﻿using Axle.Application.Services;
using Axle.Modularity;

namespace Maeson.Core.Builders;

[Module]
[Requires(typeof(ServiceRegistry))]
internal sealed class MaesonBuilderModule : ServiceGroup<MaesonBuilderModule, IMaesonBuilder>
{
    private readonly IDictionary<string, IMaesonBuilder> _builders;
    
    public MaesonBuilderModule(ServiceRegistry serviceRegistry) : base(serviceRegistry)
    {
        _builders = new Dictionary<string, IMaesonBuilder>(StringComparer.OrdinalIgnoreCase);

        foreach (var formatter in serviceRegistry)
        {
            _builders.Add(formatter.Name, formatter);
        }
    }

    public IMaesonBuilder? FindBuilder(string format) => _builders.TryGetValue(format, out var builder) ? builder : null;
}