using Axle.Application.Services;
using Axle.Modularity;

namespace Maeson.Core.Builders;

[Requires(typeof(MaesonBuilderModule.ServiceRegistry))]
[ProvidesFor(typeof(MaesonBuilderModule))]
[AttributeUsage(AttributeTargets.Class|AttributeTargets.Interface)]
internal sealed class MaesonBuilderAttribute : AbstractServiceAttribute
{
    public MaesonBuilderAttribute()
    {
        AutoExported = false;
    }
}