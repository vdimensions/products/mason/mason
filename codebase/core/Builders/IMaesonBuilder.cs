namespace Maeson.Core.Builders;

[MaesonBuilder]
public interface IMaesonBuilder
{
    void Build(IDictionary<string, string> document, Stream outputStream);
    
    string Name { get; }
}