﻿using System.Text;
using Axle.Logging;
using Axle.Modularity;
using Axle.Text.Documents;
using Axle.Text.Documents.Properties;
using Axle.Text.Documents.Yaml;
using Axle.Text.Substitution;

namespace Maeson.Core.Services;

[Module]
public sealed class DocumentService
{
    private readonly ILogger _logger;

    internal DocumentService(ILogger logger)
    {
        _logger = logger;
    }

    public ITextDocumentRoot? LoadDocument(string path)
    {
        if (!File.Exists(path))
        {
            _logger.Warn("Could not load document {0}. File does not exist.", path);
            return null;
        }
        var comparer = StringComparer.OrdinalIgnoreCase;
        var encoding = Encoding.UTF8;
        var extension = Path.GetExtension(path);
        using var stream = File.OpenRead(path);
        switch (extension)
        {
            case ".properties":
                return new PropertiesDocumentReader().Read(stream, encoding, comparer);
            case ".yml":
            case ".yaml":
                return new YamlDocumentReader().Read(stream, encoding, comparer);
        }
        
        _logger.Warn("Could not load document {0}. Unsupported file extension ({1}).", path, extension);
        return null;
    }

    public ISubstitutionProvider GetSubstitutionProvider(ITextDocumentObject[] documents, IReadOnlyList<string> placeholders)
    {
        var dictionary = new Dictionary<string, string>(StringComparer.Ordinal);
        for (var i = 0; i < placeholders.Count; i++)
        {
            var placeholder = placeholders[i];
            for (var j = 0; j < documents.Length; j++)
            {
                var value = documents[j].GetValues(placeholder).FirstOrDefault();
                if (value is ITextDocumentValue { Value: not null } documentValue)
                {
                    dictionary.Add(placeholder, documentValue.Value.ToString()!);
                    break;
                }
            }
        }
        return new DictionarySubstitutionProvider(dictionary);
    }
}