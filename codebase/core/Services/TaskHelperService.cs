﻿using System.Runtime.Serialization;
using Axle.Modularity;
using Axle.Text.Documents;
using Axle.Text.Documents.Binding;
using Maeson.Core.Builders;
using Maeson.Core.Tasks;

namespace Maeson.Core.Services;

[Module]
[Requires(typeof(DocumentService))]
[Requires(typeof(MaesonBuilderModule))]
public class TaskHelperService
{
    [DataContract]
    private record TransformInstructionHolder
    {
        [field:DataMember]
        public List<MaesonTransformInstruction> Transform { get; init; }
    }
    
    private readonly DocumentService _documentService;

    public TaskHelperService(DocumentService documentService)
    {
        _documentService = documentService;
    }

    private IDictionary<string, ITextDocumentObject> DoReadInputs(string currentDirectory, IEnumerable<MaesonInputItemSettings> inputs)
    {
        return inputs
           .SelectMany(x => Directory.GetFiles(currentDirectory, x.Path))
           .AsParallel()
           .Select(path => (Name: Path.GetFileNameWithoutExtension(path), Data: _documentService.LoadDocument(path) as ITextDocumentObject))
           .Where(tuple => tuple.Data != null)
           .ToDictionary(result => result.Name, result => result.Data!, StringComparer.Ordinal);
    }

    public IDictionary<string, ITextDocumentObject> ReadInputs(IEnumerable<MaesonInputItemSettings> inputs) => DoReadInputs(Environment.CurrentDirectory, inputs);

    public async Task<IDictionary<string, ITextDocumentObject>> ReadInputsAsync(IEnumerable<MaesonInputItemSettings> inputs)
    {
        var currentDirectory = Environment.CurrentDirectory;
        return await Task.Run(() => DoReadInputs(currentDirectory, inputs));
    }
    
    private IReadOnlyList<MaesonTransformInstruction> DoReadTransforms(string currentDirectory, IEnumerable<MaesonTransformItemSettings> transforms)
    {
        var binder = new DefaultDocumentBinder();
        var pathsArray = transforms.Select(x => Path.Combine(currentDirectory, x.Path)).ToArray();
        var keys = pathsArray.Select(Path.GetFileNameWithoutExtension).ToArray();
        var results = pathsArray
            .Select((path, i) => (Name: keys[i]!, Data: _documentService.LoadDocument(path)))
            .Where(tuple => tuple.Data != null)
            .Select(tuple => (tuple.Name, Data: (TransformInstructionHolder) binder.Bind(tuple.Data, typeof(TransformInstructionHolder))))
            .ToDictionary(result => result.Name, result => result.Data!.Transform.ToArray(), StringComparer.Ordinal);
        return keys
           .SelectMany(x => results.TryGetValue(x, out var v) ? v : Array.Empty<MaesonTransformInstruction>())
           .ToArray();
    }

    public IReadOnlyList<MaesonTransformInstruction> ReadTransforms(IEnumerable<MaesonTransformItemSettings> transforms)
    {
        return DoReadTransforms(Environment.CurrentDirectory, transforms);
    }

    public async Task<IReadOnlyList<MaesonTransformInstruction>> ReadTransformsAsync(IEnumerable<MaesonTransformItemSettings> transforms)
    {
        var currentDirectory = Environment.CurrentDirectory;
        return await Task.Run(() => DoReadTransforms(currentDirectory, transforms));
    }
}