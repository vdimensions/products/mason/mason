﻿using System.Runtime.Serialization;

namespace Maeson.Core.Tasks;

[DataContract]
public record MaesonTransformInstruction
{
    /// <summary>
    /// An expression or key to select.
    /// </summary>
    public string Select { get; init; }
    
    /// <summary>
    /// A list of input documents, that will be merged in the specified order before querying the key from
    /// the expression provided in <see cref="Select"/>.
    /// </summary>
    public List<string> From { get; init; }
    
    /// <summary>
    /// The keys that the result will be exported to.
    /// </summary>
    public List<string> Into { get; init; }
}