using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace Maeson.Core.Tasks;

[DataContract]
[StructLayout(LayoutKind.Sequential)]
public record MaesonTaskInfo
{
    [field:DataMember]
    public string Name { get; init; }
    
    [field:DataMember]
    public string Author { get; init; }
}