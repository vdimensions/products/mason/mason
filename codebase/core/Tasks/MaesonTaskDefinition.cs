﻿using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace Maeson.Core.Tasks;

[DataContract]
[StructLayout(LayoutKind.Sequential)]
public record MaesonTaskDefinition
{
    [field:DataMember]
    public MaesonTaskInfo Info { get; init; }

    [field:DataMember]
    public List<MaesonInputItemSettings> Inputs { get; init; }

    [field:DataMember]
    public List<MaesonTransformItemSettings> Transforms { get; init; }

    [field:DataMember]
    public List<MaesonOutputItemSettings> Outputs { get; init; }
}