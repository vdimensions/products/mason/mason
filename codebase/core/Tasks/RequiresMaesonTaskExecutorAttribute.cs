using Axle.Modularity;

namespace Maeson.Core.Tasks;

[AttributeUsage(AttributeTargets.Class|AttributeTargets.Interface)]
public sealed class RequiresMaesonTaskExecutorAttribute : RequiresAttribute
{
    public RequiresMaesonTaskExecutorAttribute() : base(typeof(MaesonTaskExecutorModule)) { }
}