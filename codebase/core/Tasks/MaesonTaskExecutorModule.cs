﻿using Axle.Logging;
using Axle.Modularity;
using Axle.Text.Documents;
using Axle.Text.Documents.Binding;
using Axle.Text.Substitution;
using Maeson.Core.Builders;
using Maeson.Core.Services;

namespace Maeson.Core.Tasks;

[Module]
[Requires(typeof(MaesonBuilderModule))]
[Requires(typeof(DocumentService))]
[Requires(typeof(TaskHelperService))]
internal sealed class MaesonTaskExecutorModule : IMaesonTaskExecutor
{
    private readonly DocumentService _documentService;
    private readonly TaskHelperService _taskHelperService;
    private readonly MaesonBuilderModule _builderModule;
    private readonly ILogger _logger;

    private readonly IDocumentBinder _documentBinder = new DefaultDocumentBinder();

    public MaesonTaskExecutorModule(
        DocumentService documentService, 
        TaskHelperService taskHelperService, 
        MaesonBuilderModule builderModule, 
        ILogger logger)
    {
        _documentService = documentService;
        _taskHelperService = taskHelperService;
        _builderModule = builderModule;
        _logger = logger;
    }

    public void Execute(string path)
    {
        var project = LoadProject(path);
        if (project == null)
        {
            _logger.Warn("Could not load project: {0}", path);
            return;
        }

        var directory = Path.GetDirectoryName(path)!;
        Environment.CurrentDirectory = directory;
        var inputsTask = _taskHelperService.ReadInputsAsync(project.Inputs);
        var transformsTask = _taskHelperService.ReadTransformsAsync(project.Transforms);
        var inputs = inputsTask.GetAwaiter().GetResult();
        var transforms = transformsTask.GetAwaiter().GetResult();
        var transformedDocument = ApplyTransforms(inputs, transforms);
        WriteOutputs(directory, transformedDocument, project.Outputs);
    }
    
    private void WriteOutputs(string currentDirectory, IDictionary<string, string> document, IEnumerable<MaesonOutputItemSettings> outputs)
    {
        void Action(MaesonOutputItemSettings x)
        {
            var builder = _builderModule.FindBuilder(x.Format);

            if (builder == null)
            {
                return;
            }

            var targetFile = Path.Combine(currentDirectory, x.Path);
            var directory = Path.GetDirectoryName(targetFile)!;
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            if (File.Exists(targetFile))
            {
                File.Delete(targetFile);
            }

            using var stream = File.OpenWrite(targetFile);
            builder.Build(document, stream);
        }

        outputs.AsParallel().ForAll(Action);
    }

    private IDictionary<string, string> ApplyTransforms(IDictionary<string, ITextDocumentObject> inputs, IReadOnlyList<MaesonTransformInstruction> transforms)
    {
        var result = new Dictionary<string, string>(StringComparer.Ordinal);
        foreach (var transform in transforms)
        {
            var documentsToMerge = transform.From
                .Select(
                    x =>
                    {
                        var split = x.Split("|");
                        if (inputs.TryGetValue(split[0], out var doc))
                        {
                            if (split.Length == 2)
                            {
                                return new TextDocumentSubset(doc, split[1]);
                            }
                            else
                            {
                                return doc;
                            }
                        }
                        return null;
                    })
                .Where(x => x != null)
                .ToArray();
            var processor = new StandardSubstitutionProcessor();
            var placeholders = processor.ExtractPlaceholders(transform.Select);
            var substitutionProvider = new DocumentSubstitutionProvider(_documentService.GetSubstitutionProvider(documentsToMerge!, placeholders));
            var value = processor.Replace(transform.Select, substitutionProvider);
            if (!string.IsNullOrEmpty(value))
            {
                for (var i = 0; i < transform.Into.Count; i++)
                {
                    result.Add(transform.Into[i], value);
                }
            }
        }
        return result;
    }

    private MaesonTaskDefinition? LoadProject(string path)
    {
        var document = _documentService.LoadDocument(path);
        if (document == null)
        {
            return null;
        }
        return _documentBinder.Bind(document, typeof(MaesonTaskDefinition)) as MaesonTaskDefinition;
    }

    private sealed class DocumentSubstitutionProvider : ISubstitutionProvider
    {
        private readonly ISubstitutionProvider _impl;

        public DocumentSubstitutionProvider(ISubstitutionProvider impl)
        {
            _impl = impl;
        }

        public bool TrySubstitute(string token, out string? value)
        {
            if (!_impl.TrySubstitute(token, out value))
            {
                value = string.Empty;
            }
            return true;
        }
    }
}