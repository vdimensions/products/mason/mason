using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace Maeson.Core.Tasks;

[DataContract]
[StructLayout(LayoutKind.Sequential)]
public record MaesonTransformItemSettings
{
    [field:DataMember]
    public string Path { get; init; }
}