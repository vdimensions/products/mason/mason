namespace Maeson.Core.Tasks;

public interface IMaesonTaskExecutor
{
    void Execute(string path);
}